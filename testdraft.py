'''
Run Fantasy football draft
'''
import drafttools

# who is playing
managers = ['Chengdu Smirker','SF KUKOC','Matadors','Maniacal Tanks','Ducky','Eclair','Nasty','TD Only','Raymond','DMHDF','Wisconsin Badger','Desperado']
#managers = ['DMHDF','Chengdu Smirker','SF KUKOC','Matadors','Maniacal Tanks','Ducky','Eclair','Nasty','TD Only','Raymond','Wisconsin Badger','Desperado']
#managers = ['YoungWeezy','SuperbT','SSLazio','DMHDF','WisconsinB','ericyen','Cheesehead','TUIFU','Nicholas','Wei','Robin','Eclair']

# this will be different
models=[drafttools.Model(type='VOR',draftpos=9,auto = False)]
#models=[drafttools.Model(type='ADP',draftpos=0)]

for i in xrange(11):
    models.append(drafttools.Model(type='VOR',draftpos=i+1,auto = False))
    #models.append(drafttools.Model(type='ADP',draftpos=i+1))

# init draft
thedraft = drafttools.Draft(managers,models,fullauto=False)
thedraft.start()
